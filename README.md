# ML Cadence Generator
Author : Martin Fonteyn  

# Files
ultimate_model.py : used to train the model, where you can specify the scaling and network complexity  
generate_resistances.py : script used by SKILL.py to create the resistance network used in Cadence. You can specify the resistances values  
weights_analysis.py : used when trying to avoid saturation, to compute the maximal input at each layer  
SKILL.py : generates the .txt file containing the SKILL instructions to generate the whole network. It can be loaded in cadence with the load("auto.txt") command