from enum import Enum

import numpy as np

from generate_resistances import generate_resistances


class ActivationFunctions(Enum):
    LINEAR = 0
    RELU = 1
    SIGMOID = 2


f = open("auto9.txt", "w")

global_offset_x = 0
global_offset_y = 0

bias_voltage = 1.0
input_voltages = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 * bias_voltage])

layer_count = 4
layer_activation_functions = [ActivationFunctions.RELU, ActivationFunctions.SIGMOID, ActivationFunctions.RELU, ActivationFunctions.LINEAR]
layers_input_count = [11, 129, 129, 129]
layers_output_count = [128, 128, 128, 10]

current_x = global_offset_x
current_y = global_offset_y

stage_offset_x = global_offset_x
stage_offset_y = global_offset_y

all_res_matrix_pos, all_res_matrix_neg, all_biases = generate_resistances()

for layer_number in range(layer_count):
    first_layer = layer_number == 0
    last_layer = layer_number == layer_count-1
    layer_activation_function = layer_activation_functions[layer_number]

    global_offset_x = stage_offset_x
    global_offset_y = stage_offset_y

    input_count = layers_input_count[layer_number]
    output_count = layers_output_count[layer_number]

    res_matrix_pos = all_res_matrix_pos[layer_number]
    res_matrix_neg = all_res_matrix_neg[layer_number]
    biases = all_biases[layer_number]

    for i in range(input_count):
        current_x = global_offset_x + i*0.5
        current_y = global_offset_y + i*(-0.5)

        if first_layer:
            # voltage source and ground
            f.write('source_id = dbCreateInstByMasterName(geGetEditCellView() "analogLib" "vsource" "symbol" ' + ('"Vinput_' +
                  str(i) if i != input_count - 1 else '"V_bias_0') + '" list(' + str(current_x) + ' ' + str(current_y) + ') "R0")\n')
            # set input_voltage
            f.write('schReplaceProperty(list(source_id) "vdc" "' + str(input_voltages[i] / 10) + '")\n')
            f.write('schReplaceProperty(list(source_id) "srcType" "dc")\n')
            f.write('dbCreateInstByMasterName(geGetEditCellView() "analogLib" "gnd" "symbol" "GND_' +
                  str(i) + '" list(' + str(current_x) + ' ' + str(current_y-0.3750) + ') "R0") \n')
            # create pins
            f.write('inputCVId = dbOpenCellViewByType( "basic" "ipin" "symbol" "" \'r) \n')
            f.write('schCreatePin(geGetEditCellView() inputCVId "Input_voltage_' + str(i) + '" "input" nil ' + str(current_x) + ':' + str(current_y) + ' "R0" ) \n')
        elif i == input_count - 1:
            f.write('source_id = dbCreateInstByMasterName(geGetEditCellView() "analogLib" "vsource" "symbol" ' + (
                '"Vinput_' +
                str(i) if i != input_count - 1 else '"V_bias_' + str(layer_number)) + '" list(' + str(current_x) + ' ' + str(
                current_y) + ') "R0")\n')
            # set input_voltage
            f.write('schReplaceProperty(list(source_id) "vdc" "' + str(bias_voltage) + '")\n')
            f.write('schReplaceProperty(list(source_id) "srcType" "dc")\n')
            f.write('dbCreateInstByMasterName(geGetEditCellView() "analogLib" "gnd" "symbol" "GND_bias_' +
                    str(layer_number) + '" list(' + str(current_x) + ' ' + str(current_y - 0.3750) + ') "R0") \n')
        # wire
        f.write('schCreateWire(geGetEditCellView() "draw" "flight" list(' +
              str(current_x) + ':' + str(current_y) + ' ' + str(current_x + 1.5*output_count) + ':' + str(current_y) + ') 0.0625 0.0625 0.0 ) \n')

        # resistances
        for j in range(output_count):
            current_x += 1.0
            f.write('res_id = dbCreateInstByMasterName(geGetEditCellView() "analogLib" "res" "symbol" "R_layer' + str(
                layer_number) + '_' + str(i) + '_' + str(j) + '-' +
                    '" list(' + str(current_x) + ' ' + str(current_y) + ') "R0") \n')
            f.write('res_id->r=' + str(res_matrix_neg[i][j] if layer_number == 0 else res_matrix_neg[input_count - 2 - i % input_count][j]) + '\n')
            # f.write('schReplaceProperty(list(res_id) "r" "' + str(res_matrix_neg[i][j]) + '")\n')

            current_x += 0.5
            f.write('res_id = dbCreateInstByMasterName(geGetEditCellView() "analogLib" "res" "symbol" "R_layer' + str(
                layer_number) + '_' + str(i) + '_' + str(j) + '+' +
                    '" list(' + str(current_x) + ' ' + str(current_y) + ') "R0") \n')
            f.write('res_id->r=' + str(res_matrix_pos[i][j] if layer_number == 0 else res_matrix_pos[input_count - 2 - i % input_count][j]) + '\n')
            # f.write('schReplaceProperty(list(res_id) "r" "' + str(res_matrix_pos[i][j]) + '")\n')



    # diagonal wires
    for i in range(input_count-1):
        current_y = global_offset_y - 0.3750 - i*0.5
        current_x = global_offset_x + i*0.5
        for j in range(output_count):
            current_x += 1.0
            f.write('schCreateWire(geGetEditCellView() "draw" "direct" list(' +
                  str(current_x) + ':' + str(current_y) + ' ' + str(current_x + 0.5) + ':' + str(current_y - 0.5) + ') 0.0625 0.0625 0.0 "red" ) \n')
            current_x += 0.5
            f.write('schCreateWire(geGetEditCellView() "draw" "direct" list(' +
                  str(current_x) + ':' + str(current_y) + ' ' + str(current_x + 0.5) + ':' + str(current_y - 0.5) + ') 0.0625 0.0625 0.0 "red" ) \n')

    current_x = global_offset_x + 0.5 * (input_count - 1)
    current_y = global_offset_y - 0.3750 - 0.5 * (input_count - 1)
    diagonal_length = 1.0
    output_x = 0
    output_y = 0

    mult_output_x = np.zeros(output_count)
    mult_output_y = np.zeros(output_count)

    # diagonal wires below
    for j in range(output_count):
        current_x += 1.0
        f.write('schCreateWire(geGetEditCellView() "draw" "direct" list(' + str(current_x) + ':' + str(current_y) + ' ' +
              str(current_x + diagonal_length * (output_count - j)) + ':' + str(current_y - diagonal_length * (output_count - j)) + ') 0.0625 0.0625 0.0 "red" ) \n')
        input_pin_x = current_x + diagonal_length * (output_count - j)
        input_pin_y = current_y - diagonal_length * (output_count - j)

        origin_x = input_pin_x - 3.75
        origin_y = input_pin_y - 3.0

        # multiplication block
        f.write('dbCreateInstByMasterName(geGetEditCellView() "test1" "multiplication_1" "schematic" "multiplication_layer' + str(layer_number) + '_' + str(j) +
              '" list(' + str(origin_x) + ' ' + str(origin_y) + ') "R0")\n')

        # add output wire and pin
        if j == output_count - 1:
            output_x = origin_x + 10.4375
            output_y = origin_y + 3.5

        mult_output_x[output_count - 1 - j] = origin_x + 10.4375
        mult_output_y[output_count - 1 - j] = origin_y + 3.5

        current_x += 0.5
        f.write('schCreateWire(geGetEditCellView() "draw" "direct" list(' + str(current_x) + ':' + str(current_y) + ' ' +
              str(current_x + diagonal_length * (output_count - j)) + ':' + str(current_y - diagonal_length * (output_count - j)) + ') 0.0625 0.0625 0.0 "red" ) \n')

    for j in range(output_count):
        final_output_x = output_x + j * 0.5
        final_output_y = output_y - j * 0.5

        if j != 0:
            f.write('schCreateWire(geGetEditCellView() "draw" "flight" list(' +
                  str(mult_output_x[j]) + ':' + str(mult_output_y[j]) + ' ' + str(final_output_x) + ':' + str(mult_output_y[j]) + ') 0.0625 0.0625 0.0 ) \n')
            f.write('schCreateWire(geGetEditCellView() "draw" "flight" list(' +
                  str(final_output_x) + ':' + str(mult_output_y[j]) + ' ' + str(final_output_x) + ':' + str(final_output_y) + ') 0.0625 0.0625 0.0 ) \n')

        if last_layer:
            f.write('inputCVId = dbOpenCellViewByType( "basic" "ipin" "symbol" "" \'r) \n')
            f.write('schCreatePin(geGetEditCellView() inputCVId "Output_voltage_' + str(output_count - 1 - j) + '" "output" nil ' +
                  str(final_output_x + 1.1250) + ':' + str(final_output_y) + ' "R0" ) \n')

        # activation functions
        if layer_activation_function == ActivationFunctions.LINEAR:
            f.write('activation_id = dbCreateInstByMasterName(geGetEditCellView() "test1" "linear" "symbol" "linear_layer' + str(layer_number) + '_' + str(j) + '" list(' + str(final_output_x) + ' ' + str(final_output_y) + ') "R0")\n')
            # f.write('activation_id->Voffset=' + str(biases[output_count - 1 - j]) + '\n')
            f.write('activation_id->Voffset=0\n')
            if j == 0:
                stage_offset_x = final_output_x + 1.1250
                stage_offset_y = final_output_y
        elif layer_activation_function == ActivationFunctions.RELU:
            f.write('activation_id = dbCreateInstByMasterName(geGetEditCellView() "test1" "relu" "symbol" "relu_layer' + str(layer_number) + '_' + str(j) + '" list(' + str(final_output_x) + ' ' + str(final_output_y) + ') "R0")\n')
            # f.write('activation_id->Voffset=' + str(biases[output_count - 1 - j]) + '\n')
            f.write('activation_id->Voffset=0\n')
            if j == 0:
                stage_offset_x = final_output_x + 1.1250
                stage_offset_y = final_output_y
        elif layer_activation_function == ActivationFunctions.SIGMOID:
            f.write('activation_id =  dbCreateInstByMasterName(geGetEditCellView() "test1" "sigmoid" "symbol" "sigmoid_layer' + str(layer_number) + '_' + str(j) + '" list(' + str(final_output_x) + ' ' + str(final_output_y) + ') "R0")\n')
            # f.write('activation_id->Voffset=' + str(biases[output_count - 1 - j]) + '\n')
            f.write('activation_id->Voffset=0\n')
            if j == 0:
                stage_offset_x = final_output_x + 1.1250
                stage_offset_y = final_output_y

f.close()