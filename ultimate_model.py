import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.constraints import Constraint
import keras.backend as K

# Generate random input data
from tensorflow.python.keras.utils.vis_utils import plot_model

num_samples = 10000
input_dim = 10
# input between 0 and 0.9
scaling = 0.9
output_scaling = 1.0
X_train = np.concatenate((scaling * np.random.rand(num_samples, input_dim), np.array(scaling * 0.1 * np.random.randint(0, 10, [4*num_samples, input_dim]), dtype="float64")), axis=0)
np.random.shuffle(X_train)


class ClippedSigmoid(tf.keras.layers.Layer):
    def __init__(self, clip_value, **kwargs):
        super(ClippedSigmoid, self).__init__(**kwargs)
        self.clip_value = clip_value

    def call(self, inputs, **kwargs):
        clipped_inputs = tf.clip_by_value(inputs, -1.125, self.clip_value)
        return tf.keras.activations.sigmoid(clipped_inputs)

    def get_config(self):
        config = super(ClippedSigmoid, self).get_config()
        config.update({'clip_value': self.clip_value})
        return config


class MaxCappedReLU(tf.keras.layers.Layer):
    def __init__(self, max_value, **kwargs):
        super(MaxCappedReLU, self).__init__(**kwargs)
        self.max_value = max_value

    def call(self, inputs, **kwargs):
        return tf.minimum(tf.nn.relu(inputs), self.max_value)

    def get_config(self):
        config = super(MaxCappedReLU, self).get_config()
        config.update({'max_value': self.max_value})
        return config


# Define custom constraints for weights and biases
class WeightConstraint(Constraint):
    def __init__(self, min_value, max_value):
        self.min_value = min_value
        self.max_value = max_value

    def __call__(self, w):
        return K.clip(w, self.min_value, self.max_value)

    def get_config(self):
        return {'min_value': self.min_value, 'max_value': self.max_value}


class BiasConstraint(Constraint):
    def __init__(self, min_value, max_value):
        self.min_value = min_value
        self.max_value = max_value

    def __call__(self, b):
        return K.clip(b, self.min_value, self.max_value)

    def get_config(self):
        return {'min_value': self.min_value, 'max_value': self.max_value}


# Generate labels using softmax function
def softmax(x):
    exp_x = np.exp(x)
    return exp_x / np.sum(exp_x, axis=1, keepdims=True)


y_train = softmax(X_train) * output_scaling

# Define a simple neural network model
model = keras.Sequential([
    keras.layers.Dense(128, input_shape=(input_dim,), activation=MaxCappedReLU(1.2), kernel_constraint=WeightConstraint(min_value=-0.2, max_value=0.2), bias_constraint=BiasConstraint(min_value=-0.1, max_value=0.1)),
    keras.layers.Dense(128, activation=ClippedSigmoid(0.465), kernel_constraint=WeightConstraint(min_value=-0.2, max_value=0.2), bias_constraint=BiasConstraint(min_value=-0.1, max_value=0.1)),
    keras.layers.Dense(128, activation=MaxCappedReLU(0.465), kernel_constraint=WeightConstraint(min_value=-0.2, max_value=0.2), bias_constraint=BiasConstraint(min_value=-0.1, max_value=0.1)),
    keras.layers.Dense(10, activation='linear', kernel_constraint=WeightConstraint(min_value=-0.2, max_value=0.2), bias_constraint=BiasConstraint(min_value=-0.1, max_value=0.1))
])

# Compile the model
model.compile(optimizer='adam',
              loss='mean_absolute_error',
              metrics=['mse'])

# Train the model
class PrintResults(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        if epoch % 20 == 0:
            print("Epoch:", epoch)
            print("Loss:", logs['loss'])
            print("Accuracy:", logs['mse'])
        # if epoch % 500 == 0:
            # model.save_weights("D:\Onedrive\hdd portable\dèv\Python\softmax\\weights\\" + str(epoch))

# Train the model with the custom callback
# model.load_weights("D:\Onedrive\hdd portable\dèv\Python\softmax\\weights\\ultimate_4")
model.fit(X_train, y_train, epochs=500, batch_size=128, callbacks=[PrintResults()], verbose=False)

X_test = scaling * np.random.rand(10000, input_dim)
y_test = softmax(X_test) * output_scaling

x = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0.9]]
y = model.predict(np.array(x, dtype="float64"))
print(y)
print(softmax(x))

# Evaluate the model
loss, accuracy = model.evaluate(X_test, y_test)
print("Loss:", loss)
print("Accuracy:", accuracy)
model.save_weights("D:\Onedrive\hdd portable\dèv\Python\softmax\\weights\\ultimate_6")