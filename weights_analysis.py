import numpy as np
from tensorflow import keras

input_dim = 10

def softmax(x):
    exp_x = np.exp(x)
    return exp_x / np.sum(exp_x, axis=1, keepdims=True)

# Define a simple neural network model
model = keras.Sequential([
    keras.layers.Dense(128, input_shape=(input_dim,), activation='relu'),
    keras.layers.Dense(128, activation='sigmoid'),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='linear')
])

# Compile the model
model.compile(optimizer='adam',
              loss='mean_absolute_error',
              metrics=['mse'])

model.load_weights("D:\Onedrive\hdd portable\dèv\Python\softmax\\weights\\ultimate_3")

# Initialize variables to store maximum and minimum values
max_value = float('-inf')  # Initialize to negative infinity
min_value = float('inf')  # Initialize to positive infinity

weights = model.get_weights()
max_output = []
min_output = []

max_input = 0.9  # so that the first layer is ok
last_layer_max_input = [max_input] * 10
last_layer_min_input = [0.0] * 10

# only considering positive inputs for now
for index in range(len(weights) // 2):
    layer_weights = np.transpose(weights[2 * index])
    layer_bias = weights[2 * index + 1]
    layer_max_output = []
    layer_min_output = []

    for j in range(len(layer_bias)):
        max_sum = 0
        min_sum = 0
        input_weights = layer_weights[j]

        for k in range(len(input_weights)):
            if input_weights[k] > 0:
                max_sum += max(last_layer_min_input[k], last_layer_max_input[k]) * input_weights[k]
            else:
                min_sum += min(last_layer_min_input[k], last_layer_max_input[k]) * input_weights[k]

        if index == 1:
            layer_min_output.append(max(layer_bias[j] + min_sum, 0))  # sigmoid returns minimum 0 (this is a simplification of the sigmoid but it is good
            layer_max_output.append(min(layer_bias[j] + max_sum, 1))  # sigmoid returns maximum 1
        else:
            layer_min_output.append(max(layer_bias[j] + min_sum, 0))  # relu returns minimum 0
            layer_max_output.append(max(layer_bias[j] + max_sum, 0))

    last_layer_max_input = layer_max_output
    last_layer_min_input = layer_min_output
    print("Largest possible output after layer " + str(index) + " is " + str(np.max(layer_max_output)))
    print("Smallest possible output after layer " + str(index) + " is " + str(np.min(layer_min_output)))
    max_output.append(layer_max_output)
    min_output.append(layer_min_output)

x = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0.9]]
y = model.predict(np.array(x, dtype="float64"))
print(y)

print("end")