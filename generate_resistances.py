import math

import numpy as np
from tensorflow import keras

def generate_resistances():

    input_dim = 10

    model = keras.Sequential([
        keras.layers.Dense(128, input_shape=(input_dim,), activation='relu'),
        keras.layers.Dense(128, activation='sigmoid'),
        keras.layers.Dense(128, activation='relu'),
        keras.layers.Dense(10, activation='linear')
    ])

    # Compile the model
    model.compile(optimizer='adam',
                  loss='mean_absolute_error',
                  metrics=['mse'])

    model.load_weights("D:\Onedrive\hdd portable\dèv\Python\softmax\\weights\\ultimate_6")
    weights = model.get_weights()

    Rf = 10000  # for max sensitivity
    Rmin = 1000
    Rmax = 10000

    layers_input_count = [10, 128, 128, 128]
    layers_output_count = [128, 128, 128, 10]

    res_matrix_pos = []
    res_matrix_neg = []
    biases = []

    for index in range(len(weights)//2):
        layer_weights = weights[2*index]
        # if index == 0:
        #    layer_weights = layer_weights * 10  # was trained for 0 to 9, but the input will be from 0 to 0.9
        arr_max = np.max(layer_weights)
        arr_min = np.min(layer_weights)
        if arr_max > 9:
            raise "Weight is too large"
        if arr_min < -9:
            raise "Weight is too small"
        print("Layer max weight is " + str(arr_max) + ", layer min weight is " + str(arr_min))

        # computing the resistance
        # general formula : weight = Rf * (1/r+ - 1/r-)

        res_matrix_pos.append(np.zeros((len(layer_weights) + 1, len(layer_weights[0]))))
        res_matrix_neg.append(np.zeros((len(layer_weights) + 1, len(layer_weights[0]))))
        biases.append(np.zeros(len(layer_weights)))

        for input_index, input_weights in enumerate(layer_weights):
            for output_index, weight in enumerate(input_weights):
                assert weight != 0
                if weight > 0:
                    r_minus = 10000  # lower error
                    r_plus = round(1/(weight/Rf + 1/r_minus))
                    assert Rmin <= r_plus <= Rmax
                    error = abs(weight - Rf * (1/r_plus - 1/r_minus))
                    assert error < 0.002
                    res_matrix_pos[index][input_index][output_index] = r_plus
                    res_matrix_neg[index][input_index][output_index] = r_minus
                else:
                    r_plus = 10000  # lower error
                    r_minus = round(-1 / (weight / Rf - 1 / r_plus))
                    assert Rmin <= r_minus <= Rmax
                    error = abs(weight - Rf * (1 / r_plus - 1 / r_minus))
                    assert error < 0.002
                    res_matrix_pos[index][input_index][output_index] = r_plus
                    res_matrix_neg[index][input_index][output_index] = r_minus

        # adding bias as resistances
        for output_index, bias in enumerate(weights[2*index + 1]):
            if bias == 0:
                res_matrix_pos[index][len(layer_weights)][output_index] = 10000
                res_matrix_neg[index][len(layer_weights)][output_index] = 10000
            elif bias > 0:
                r_minus = 10000  # lower error
                r_plus = round(1 / (bias / Rf + 1 / r_minus))
                assert Rmin <= r_plus <= Rmax
                error = abs(bias - Rf * (1 / r_plus - 1 / r_minus))
                assert error < 0.002
                res_matrix_pos[index][len(layer_weights)][output_index] = r_plus
                res_matrix_neg[index][len(layer_weights)][output_index] = r_minus
            else:
                r_plus = 10000  # lower error
                r_minus = round(-1 / (bias / Rf - 1 / r_plus))
                assert Rmin <= r_minus <= Rmax
                error = abs(bias - Rf * (1 / r_plus - 1 / r_minus))
                assert error < 0.002
                res_matrix_pos[index][len(layer_weights)][output_index] = r_plus
                res_matrix_neg[index][len(layer_weights)][output_index] = r_minus

        layer_bias = weights[2*index + 1]
        biases[index] = layer_bias

        arr_max = np.max(layer_bias)
        arr_min = np.min(layer_bias)
        if arr_max > 9:
            raise "Bias is too large"
        if arr_min < -9:
            raise "Bias is too small"
        print("Layer max bias is " + str(arr_max) + ", layer min bias is " + str(arr_min))

    return res_matrix_pos, res_matrix_neg, biases